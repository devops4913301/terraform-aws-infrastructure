provider "aws" {
  region     = "us-east-1"
}

# Creating the VPC
resource "aws_vpc" "MyVPC" {
  cidr_block       = var.aws_vpc
  instance_tenancy = "default"
  tags = {
    Name = "Main"
  }
}
#Creating The Private and Public Subnets

resource "aws_subnet" "Publicsubnet" {
  vpc_id                  = aws_vpc.MyVPC.id
  cidr_block              = var.Public_Subnet
  availability_zone       = element(var.availability_zone, 0)
  map_public_ip_on_launch = true
  tags = {
    Name = "Public-Subnet"
  }
}
resource "aws_route_table_association" "Public-Sub-Asso" {
  subnet_id      = aws_subnet.Publicsubnet.id
  route_table_id = aws_route_table.PublicRT.id

}
resource "aws_route_table" "PublicRT" {
  vpc_id = aws_vpc.MyVPC.id

  tags = {
    Name = "Public-Subnet-RT"
  }
}
resource "aws_subnet" "Privatesubnet" {
  vpc_id            = aws_vpc.MyVPC.id
  cidr_block        = var.Private_Subnet
  availability_zone = element(var.availability_zone, 1)
  tags = {
    Name = "Private-Subnet"
  }
}
resource "aws_route_table" "PrivatRT" {
  vpc_id = aws_vpc.MyVPC.id
  route  = []
  tags = {
    Name = "Private-Subnet-RT"
  }
}
resource "aws_route_table_association" "Privet-Sub-Asso" {
  subnet_id      = aws_subnet.Privatesubnet.id
  route_table_id = aws_route_table.PrivatRT.id
}
#Creating the Internet Gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.MyVPC.id

  tags = {
    Name = "Main-IGW"
  }
}
resource "aws_route" "Public-RT" {
  destination_cidr_block = "0.0.0.0/0"
  route_table_id         = aws_route_table.PublicRT.id
  gateway_id             = aws_internet_gateway.gw.id
}

resource "aws_key_pair" "TF_Key" {
  key_name   = "TF_Key"
  public_key = tls_private_key.rsa.public_key_openssh
}
resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}
resource "local_file" "TF-key" {
  content  = tls_private_key.rsa.private_key_pem
  filename = "TF_Key"
}

resource "aws_key_pair" "TF-key-1" {
  key_name   = "TF_Key-1"
  public_key = tls_private_key.rsa.public_key_openssh
}
resource "local_file" "TF-key-1" {
  content  = tls_private_key.rsa.private_key_pem
  filename = "TF_Key-1"
}
#Creating the EC2 Instance
resource "aws_instance" "Jenkins-Master" {
  ami                         = element(var.AMI, 0)
  instance_type               = var.instance_type
  availability_zone           = element(var.availability_zone, 0)
  key_name                    = "TF_Key"
  subnet_id                   = aws_subnet.Publicsubnet.id
  vpc_security_group_ids      = [aws_security_group.demo-sg.id]
  associate_public_ip_address = true
  user_data                   = file("install_Jenkins.sh")
  provisioner "local-exec" {
    command = "echo '${tls_private_key.rsa.private_key_pem}' > C:/Terraform Practice/AWS Projects/InfrastructureModule/TF_Key.ppk"
  }
  tags = {
    Name = "Jenkins-Master"
  }
}
resource "aws_instance" "Ansible-Master" {
  ami                    = element(var.AMI, 0)
  instance_type          = var.instance_type
  availability_zone      = element(var.availability_zone, 1)
  key_name               = "TF_Key-1"
  subnet_id              = aws_subnet.Privatesubnet.id
  vpc_security_group_ids = [aws_security_group.demo-sg.id]
  user_data              = file("install_Ansible.sh")
  provisioner "local-exec" {
    command = "echo '${tls_private_key.rsa.private_key_pem}' > C:/Terraform Practice/AWS Projects/InfrastructureModule/TF_Key.ppk"
  }
  associate_public_ip_address = false
  tags = {
    Name = "Ansible-Master"
  }
}
locals {
  inbound_ports = [22, 80, 8080]
}
resource "aws_security_group" "demo-sg" {
  name        = "sec-grp"
  vpc_id      = aws_vpc.MyVPC.id
  description = "Allow HTTP and SSH traffic via Terraform"

ingress {
   description = "Allowing SSH Connection"
   from_port   = element(var.sg_ports,0)
   to_port     = element(var.sg_ports,0)
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }

 ingress {
   description = "Allowing HTTP Connection"
   from_port   = element(var.sg_ports,1)
   to_port     = element(var.sg_ports,1)
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }
 ingress {
   description = "Allowing HTTPs Connection"
   from_port   = element(var.sg_ports,2)
   to_port     = element(var.sg_ports,2)
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }
 ingress {
   description = "Allowing Jenkins Connection"
   from_port   = element(var.sg_ports,3)
   to_port     = element(var.sg_ports,3)
   protocol    = "tcp"
   cidr_blocks = ["0.0.0.0/0"]
 }

egress {
   from_port   = 0
   to_port     = 0
   protocol    = "-1"
   cidr_blocks = ["0.0.0.0/0"]
 }
}


/*resource "aws_network_acl" "main" {
  vpc_id = aws_vpc.MyDemoVPC.id
  subnet_ids = [aws_subnet.Publicsubnet.id]

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "icmp"
    rule_no    = 100
    action     = "allow"
    cidr_block = "10.10.1.0/24"
    from_port  = 0
    to_port    = 8
  }
}*/