variable "aws_vpc" {
  type    = string
  default = "10.10.0.0/16"
}
variable "Private_Subnet" {
  type    = string
  default = "10.10.1.0/24"
}
variable "Public_Subnet" {
  type    = string
  default = "10.10.2.0/24"
}
variable "AMI" {
  type    = list(any)
  default = ["ami-0cf10cdf9fcd62d37"]
}
variable "instance_type" {
  type    = string
  default = "t2.micro"
}
variable "availability_zone" {
  type    = list(any)
  default = ["us-east-1a", "us-east-1b"]
}

variable "sg_ports" {
  type = list(any)
  description = "List of Ingress Ports"
  default = [22,80,443,8080]
  
}


